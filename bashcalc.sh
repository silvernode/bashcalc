#!/bin/bash


addition(){

  while true;do
    clear
    echo "Toolbar: [b] - Previous Page | [q] - Quit"
    echo
    echo "Addition"
    echo "=============="
    echo
    echo -n "Input first number -> "
    read num1
    
    
    
    if [ ${num1} = b ];then
      main
    elif [ ${num1} = q ];then
      exit 0;
    else
      echo -n "Input second number -> "
      read num2
      echo
      echo "The sum of ${num1} and ${num2} is:"
      echo
      expr ${num1} + ${num2}
    
      echo
    
      echo
    
      read -n 1 -p "Press any key to try again"
      echo
    fi
    
  done
  



}

multiplication(){

  while true;do
    clear
    echo "Toolbar: [b] - Previous Page | [q] - Quit"
    echo
    echo "Multiplication"
    echo "=============="
    echo
    echo -n "Input first number -> "
    read num1
    
    
    
    if [ ${num1} = b ];then
      main
    elif [ ${num1} = q ];then
      exit 0;
    else
      echo -n "Input second number -> "
      read num2
      echo
      echo "The sum of ${num1} and ${num2} is:"
      echo
      expr ${num1} \* ${num2}
    
      echo
    
      echo
    
      read -n 1 -p "Press any key to try again"
      echo
    fi
    
  done
  
}
subtraction(){

  while true;do
    clear
    echo "Toolbar: [b] - Previous Page | [q] - Quit"
    echo
    echo "Subraction"
    echo "=============="
    echo
    echo -n "Input first number -> "
    read num1
    
    
    
    if [ ${num1} = b ];then
      main
    elif [ ${num1} = q ];then
      exit 0;
    else
      echo -n "Input second number -> "
      read num2
      echo
      echo "The sum of ${num1} and ${num2} is:"
      echo
      expr ${num1} - ${num2}
    
      echo
    
      echo
    
      read -n 1 -p "Press any key to try again"
      echo
    fi
    
  done
  
}

division(){

  while true;do
    clear
    echo "Toolbar: [b] - Previous Page | [q] - Quit"
    echo
    echo "Division"
    echo "=============="
    echo
    echo -n "Input first number -> "
    read num1
    
    
    
    if [ ${num1} = b ];then
      main
    elif [ ${num1} = q ];then
      exit 0;
    else
      echo -n "Input second number -> "
      read num2
      echo
      echo "The sum of ${num1} and ${num2} is:"
      echo
      expr ${num1} / ${num2}
    
      echo
    
      echo
    
      read -n 1 -p "Press any key to try again"
      echo
    fi
    
  done
  



}



main(){
  while true;do
    clear
    echo "BASH CALCULATOR"
    echo "==================="
  
    echo "Key: [a] - Add, [s] - Subtract, [m] - Multiply, [d] - Divide"
    echo
    echo -n "Enter an operator -> "
    read text
    
    if [ ${text} = "a" ];then
      addition
    
    elif [ ${text} = "s" ];then
      subtraction
      
    elif [ ${text} = "m" ];then
      multiplication
      
    elif [ ${text} = "d" ];then
      division
      
     elif [ ${text} = "q" ];then
      exit 0;
      
    else
      echo "Invalid option!"
      echo -n 1 -p "Press any key to try again..."
    fi
  done
}

main
